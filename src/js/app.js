import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  let pCollection = document.querySelectorAll('p');

  for(let i = 0; i < pCollection.length; i++) {
    if(pCollection[i].classList.contains('hot')){
      pCollection[i].textContent += ' 🔥';
    }
  }
});
